api = 2
core = 7.x

; Modules
projects[l10n_client][download][branch] = 7.x-1.x
projects[l10n_client][download][type] = git
projects[l10n_client][patch][2191771] = https://drupal.org/files/issues/l10n_client-browser_is_undefined_jquery_gt_19-2191771-3.patch
projects[l10n_client][subdir] = contrib
projects[tmgmt][download][branch] = 7.x-1.x
projects[tmgmt][download][type] = git
projects[tmgmt][subdir] = contrib
projects[tmgmt_google][download][branch] = 7.x-1.x
projects[tmgmt_google][download][type] = git
projects[tmgmt_google][subdir] = contrib
projects[tmgmt_microsoft][download][branch] = 7.x-1.x
projects[tmgmt_microsoft][download][type] = git
projects[tmgmt_microsoft][subdir] = contrib
